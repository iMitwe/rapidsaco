"use strict";

var MONTHS = [
  {
    "name": "January",
    "short": "Jan",
    "number": 1,
    "days": 31
  },
  {
    "name": "February",
    "short": "Feb",
    "number": 2,
    "days": 28
  },
  {
    "name": "March",
    "short": "Mar",
    "number": 3,
    "days": 31
  },
  {
    "name": "April",
    "short": "Apr",
    "number": 4,
    "days": 30
  },
  {
    "name": "May",
    "short": "May",
    "number": 5,
    "days": 31
  },
  {
    "name": "June",
    "short": "Jun",
    "number": 6,
    "days": 30
  },
  {
    "name": "July",
    "short": "Jul",
    "number": 7,
    "days": 31
  },
  {
    "name": "August",
    "short": "Aug",
    "number": 8,
    "days": 31
  },
  {
    "name": "September",
    "short": "Sep",
    "number": 9,
    "days": 30
  },
  {
    "name": "October",
    "short": "Oct",
    "number": 10,
    "days": 31
  },
  {
    "name": "November",
    "short": "Nov",
    "number": 11,
    "days": 30
  },
  {
    "name": "December",
    "short": "Dec",
    "number": 12,
    "days": 31
  }
]

var app = angular.module('sacodeapp', []);

app.controller('FilterCtrl', ['$scope', '$http', function($scope, $http) {
        $scope.years = [{'name': 2016, 'id': 2016}, {'name': 2017, 'id': 2017}, {'name': 2018, 'id': 2018}]
        $scope.update_year = function () {
              $scope.month = '';
              $http.get("/sacodeapp/" + $scope.year.id + "/undefined/")
              .then(function (response) {
                  if (response.data) {
                    console.log(response.data);
                    $scope.reports = response.data;
                    $scope.months = MONTHS;
                  } else {
                  }
              });  
      };

      // update months
        $scope.update_month = function () {
            console.log($scope.month);
            $http.get("/sacodeapp/" + $scope.year.id + "/" + $scope.month.number + "/")
              .then(function (response) {
                  if (response.data) {
                    $scope.reports = response.data;
                  } else {
                  }
              });    
      };
}]);