# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-10-02 13:44
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orgs', '0036_ensure_anon_user_exists'),
    ]

    operations = [
        migrations.AlterField(
            model_name='org',
            name='brand',
            field=models.CharField(default='sacode.org', help_text='The brand used in emails', max_length=128, verbose_name='Brand'),
        ),
    ]
