from django import forms
from temba.msgs.models import Msg


class MyForm(forms.ModelForm):
    class Meta:
        model = Msg
        widgets = {
            'target_year': forms.TextInput(
                                        attrs={
                                                'class': 'form_datetime',
                                            }
                    ),
            }
