# -*- coding: utf-8 -*-
from rest_framework import serializers
from temba.channels.models import SacodeReport
from temba.contacts.models import ContactGroup
import datetime


class ReportSerializer(serializers.ModelSerializer):
    created_on = serializers.SerializerMethodField()

    class Meta:
        model = SacodeReport
        fields = ('group', 'year', 'month', 'incoming', 'outgoing', 'created_on', )

    def get_created_on(self, obj):
        if obj.group in ['Lumitel', 'Econetleo', 'All Contacts']:
            return datetime.datetime(2016, 12, 01)
        else:
            group = ContactGroup.all_groups.filter(name=obj.group)
            if group:
                return group[0].created_on
